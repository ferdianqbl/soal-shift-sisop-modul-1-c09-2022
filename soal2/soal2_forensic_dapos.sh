#! /bin/bash

# create folder forensic_log_website_daffainfo_log
if [ ! -d "forensic_log_website_daffainfo_log" ]; then
  mkdir forensic_log_website_daffainfo_log
fi

# create avg and ip file
avgFile="./forensic_log_website_daffainfo_log/ratarata.txt"
ipFile="./forensic_log_website_daffainfo_log/result.txt"

# SubSoal B
awk -F':' '{print $3}' log_website_daffainfo.log | uniq -c | awk '{sum+=$1} END {printf "Rata-rata serangan adalah sebanyak %f requests per jam\n", (sum-1)/(NR-1)}' > $avgFile

# SubSoal C
awk -F':' '{print $1}' log_website_daffainfo.log | sort | uniq -c | sort -rn | awk -F'"' '{print $1 $2}' | awk 'NR==1 {printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", $2, $1}' > $ipFile

# SubSoal D
awk '/curl/{print}' log_website_daffainfo.log | awk 'END {print "Ada "NR" requests yang menggunakan curl sebagai user-agent"}' >> $ipFile

# SubSoal E
awk '/22\/Jan\/2022:02/ {print}' log_website_daffainfo.log | awk -F':' '{print $1}' | sort | uniq -c | sort -rn | awk '{print $2}' | awk -F'"' '{print ""$1""$2" Jam 2 pagi"}' >> $ipFile