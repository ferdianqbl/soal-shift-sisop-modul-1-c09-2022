
#!/bin/bash

# ===Find And Write Every Mem and Swap size===
MemTotal=$(free -m | awk '/^Mem/{print $2 }')

MemUsed=$(free -m | awk '/^Mem/{print $3 }')

MemFree=$(free -m | awk '/^Mem/{print $4 }')

MemShare=$(free -m | awk '/^Mem/{print $5 }')

MemAvail=$(free -m | awk '/^Mem/{print $6 }')

SwapTotal=$(free -m | awk '/^Swap/{print $2 }')

SwapUsed=$(free -m | awk '/^Swap/{print $3 }')

SwapFree=$(free -m | awk '/^Swap/{print $4 }')

# ===Writing Date and Time===
Time=$(date '+%Y%m%d%H%M%S')

# === Creating direct file===
echo "MemTotal,MemUsed,MemFree,MemShare,MemAvail,SwapTotal,SwapUsed,SwapFree" > metrics_$Time.log

echo "$MemTotal,$MemUsed,$MemFree,$MemShare,$MemAvail,$SwapTotal,$SwapUsed,$SwapFree" >> metrics_$Time.log
 
