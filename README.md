# Kelompok C09
* 5025201165 - Gabriel Solomon Sitanggang
* 5025201102 - Arya Nur Razzaq
* 5025201020 - Muhammad Ferdian Iqbal
<br><br>

# Soal 1
## Persoalan
* Han membuat sistem register pada script **register.sh** dan setiap user yang berhasil didaftarkan disimpan di dalam file **./users/user.txt.** Han juga membuat sistem login yang dibuat di script **main.sh**
* Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut :
  - Minimal 8 karakter
  - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
  - Alphanumeric
  - Tidak boleh sama dengan username
* Setiap percobaan login dan register akan tercatat pada log.txt dengan format : **MM/DD/YY hh:mm:ss MESSAGE.** Message pada log akan berbeda tergantung aksi yang dilakukan user.
  - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah **REGISTER: ERROR User already exists**
  - Ketika percobaan register berhasil, maka message pada log adalah **REGISTER: INFO User USERNAME registered successfully**
  - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah **LOGIN: ERROR Failed login attempt on user USERNAME**
  - Ketika user berhasil login, maka message pada log adalah **LOGIN: INFO User USERNAME logged in**
* Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
  - dl N *( N = Jumlah gambar yang akan didownload)*
    - Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama **YYYY-MM-DD_USERNAME**. Gambar-gambar yang didownload juga memiliki **format nama PIC_XX, dengan nomor yang berurutan** (contoh : PIC_01,
  PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
  - att
    - Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari
  user yang sedang login saat ini.


  
## Penyelesaian
1. Membuat script pada **register.sh**
``` sh
#! /bin/sh
printf "\n ====== Welcome to register Page ======\n"

printf "Please enter your username: "
read username
printf "Please enter your password: "
read -s password

printf "\n"

# === VALIDATION ===
# i. Minimal 8 karakter
if [ ${#password} -lt 8 ]
then
  printf "Password must be at least 8 characters long\n"
  exit 1
fi

# ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
if [[ ! "$password" =~ [[:upper:]] ]] 
then
  printf "uppercase character not found\n"
  exit 1
fi

if [[ ! "$password" =~ [[:lower:]] ]] 
then
  printf "lowercase character not found\n"
  exit 1
fi

# iii. Alphanumeric
if [[ "$password" =~ [^a-zA-Z0-9] ]] 
then
  printf "password must alphanumeric\n"
  exit 1
fi


# iv. Tidak boleh sama dengan username
if [ "$username" == "$password" ]
then
  printf "username and password must not be same\n"
  exit 1
fi

# === CHECK IF users FOLDER AND user.txt !EXIST ===
if [ ! -d "users" ]
then
  mkdir users
fi

if [ ! -f "log.txt" ]
then
  touch log.txt
fi

cd users

if [ ! -f "user.txt" ]
then
  touch user.txt
fi

# === CHECK IF username EXIST ===
if grep -wq "$username" user.txt
then
    date +"%D %T REGISTER: ERROR User already exists." >> ../log.txt
    exit 1
fi

# === FINAL RESULT ===
echo "username: $username" >> user.txt
echo "password: $password" >> user.txt
echo "" >> user.txt
date +"%D %T REGISTER: INFO User $username registered successfully." >> ../log.txt
```
> Pengecekan subsoal pertama
- Seperti pada command-command yang sudah ada pada script, kita meminta user untuk input username dan password yang diinginkan. Setelah itu, validasi dilakukan. Validasi yang pertama adalah memastikan jika password yang diinputkan minimal 8 karakter. Jika benar, akan dilakukan pengecekan syarat kedua, yaitu minimal 1 huruf kapital dan kecil. Setelah itu, dilakukan pengecekan Alphanumeric, lalu password juga tidak boleh sama dengan username. 
`apabila tiap pengecekan salah, langsung keluar program.`
<br><br>
> pengecekan subsoal kedua
``` bash
# === CHECK IF users FOLDER AND user.txt !EXIST ===
if [ ! -d "users" ]
then
  mkdir users
fi

if [ ! -f "log.txt" ]
then
  touch log.txt
fi

cd users

if [ ! -f "user.txt" ]
then
  touch user.txt
fi
```
- Setelah semua subsoal pertama sukses, langkah selanjutnya adalah membuat folder **users** dan file **user.txt** serta **log.txt**

``` bash
# === CHECK IF username EXIST ===
if grep -wq "$username" user.txt
then
    date +"%D %T REGISTER: ERROR User already exists." >> ../log.txt
    exit 1
fi
```
- Setelah itu, dilakukan pengecekan apakah username pernah terdaftar. Jika sudah pernah terdaftar, akan dituliskan 
**REGISTER: ERROR User already exists.** beserta waktunya pada file **log.txt**.
``` bash
# === FINAL RESULT ===
echo "username: $username" >> user.txt
echo "password: $password" >> user.txt
echo "" >> user.txt
date +"%D %T REGISTER: INFO User $username registered successfully." >> ../log.txt
```
- Apabila semua syarat selesai dan sukses, username dan password baru akan dituliskan di file **user.txt** dan laporannya dituliskan di **log.txt**, yaitu **REGISTER: INFO User USERNAME registered successfully."** beserta waktunya.
<br><br>

2. Membuat script di **main.sh**
``` bash
#! /bin/bash
printf "\n ====== Welcome to Main Page ======\n"

printf "Please enter your username: "
read username
printf "Please enter your password: "
read -s password
printf "\n"

# login status
isLogin=0

# create log.txt if not exist
if [ ! -f "log.txt" ]
then
  touch log.txt
fi

# check login status
if grep -wq "password: $password" ./users/user.txt
then
    date +"%D %T LOGIN: INFO User $username logged in." >> log.txt
    isLogin=1
else
    date +"%D %T LOGIN: ERROR Failed login attempt on user $username." >> log.txt # Password not same
    exit 1
fi

# success login
if [ $isLogin==1 ]; then
  # choose menu
  read -p "Choose your option: (att / dl 1 - ~) or (exit) " option num
  num="${num:=0}"
  
    # att
    if [ $option == "att" ]; then
      grep -wc "LOGIN:.*$username" log.txt
    fi

    # dl 
    if [ $num -gt 0 ]; then
      dir_name="$(date +"%Y-%m-%d")_$username"
      mkdir $dir_name
      cd $dir_name

      # download img
      for ((i=1;i<=$num;i++))
        do
          if [ $i -lt 10 ]; then
            wget -O "PIC_0$i.jpeg" "https://loremflickr.com/320/240"
          else
            wget -O "PIC_$i.jpeg" "https://loremflickr.com/320/240"
          fi
      done

      cd ..
    
      # zip folder
      if [ -f "$(date +"%Y-%m-%d")_$username.zip" ]
      then
        unzip -P $password $dir_name".zip"
        rm $dir_name".zip"
        zip -P $password -r "$(date +"%Y-%m-%d")_$username.zip" $dir_name
      else
        zip -P $password -r "$(date +"%Y-%m-%d")_$username.zip" $dir_name
      fi
    fi
fi
```

> Setelah user memasukkan username dan passwordnya, terdapat code **isLogin=0** sebagai status jika user belum sukses login. Setelah itu, membuat file **log.txt** apabila belum ada.

``` bash
# check login status
if grep -wq "password: $password" ./users/user.txt
then
    date +"%D %T LOGIN: INFO User $username logged in." >> log.txt
    isLogin=1
else
    date +"%D %T LOGIN: ERROR Failed login attempt on user $username." >> log.txt # Password not same
    exit 1
fi

```
> Setelah itu, dilakukan pengecekan pada password user. Apabila password tidak sesuai, akan dibuat laporan di **log.txt**, yaitu **LOGIN: ERROR Failed login attempt on user USERNAME beserta waktunya.**Jika password sesuai, **isLogin=1** dan dilaporkan ke **log.txt**, **LOGIN: INFO User USERNAME logged in**.

``` bash
# att
    if [ $option == "att" ]; then
      grep -wc "LOGIN:.*$username" log.txt
    fi
```
> Apabila **isLogin==1** user akan diminta menginputkan command antara **att dan dl N**. Jika user input command **att** akan dilakukan penghitungan user tersebut melakukan percobaan login, yang terdapat di **log.txt**.

``` bash
# att
    if [ $option == "att" ]; then
      grep -wc "LOGIN:.*$username" log.txt
    fi

    # dl 
    if [ $num -gt 0 ]; then
      dir_name="$(date +"%Y-%m-%d")_$username"
      mkdir $dir_name
      cd $dir_name

      # download img
      for ((i=1;i<=$num;i++))
        do
          if [ $i -lt 10 ]; then
            wget -O "PIC_0$i.jpeg" "https://loremflickr.com/320/240"
          else
            wget -O "PIC_$i.jpeg" "https://loremflickr.com/320/240"
          fi
      done

      cd ..
    
      # zip folder
      if [ -f "$(date +"%Y-%m-%d")_$username.zip" ]
      then
        unzip -P $password $dir_name".zip"
        rm $dir_name".zip"
        zip -P $password -r "$(date +"%Y-%m-%d")_$username.zip" $dir_name
      else
        zip -P $password -r "$(date +"%Y-%m-%d")_$username.zip" $dir_name
      fi
    fi
```

> Selanjutnya, apabila user input command **dl N (N berupa angka)**, maka gambar akan didownload sebanyak N kali.
<br>
Sebelumnya akan dibuat folder sesuai dengan user yang login dan format nama tiap gambar yang didownload adalah PIC_XX dan dimasukkan ke folder tersebut. Setelah selesai, akan dilakukan zip pada folder tersebut. Namun, apabila sebelumnya terdapat file zip dengan nama sama, akan dilakukan unzip dan dilakukan penambahan gambar, kemudian dilakukan zip kembali.
<br><br>

# Soal 2
## Persoalan
- Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
- Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
- Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
- Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
- Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

> Subsoal 1
``` bash
# create folder forensic_log_website_daffainfo_log
if [ ! -d "forensic_log_website_daffainfo_log" ]; then
  mkdir forensic_log_website_daffainfo_log
fi

# create avg and ip file
avgFile="./forensic_log_website_daffainfo_log/ratarata.txt"
ipFile="./forensic_log_website_daffainfo_log/result.txt"
```

Code ini berfungsi untuk membuat folder **forensic_log_website_daffainfo_log** apabila belum ada dan assign variable yang berisi lokasi file **ratarata.txt** dan **result.txt**.

> Subsoal 2
``` bash
awk -F':' '{print $3}' log_website_daffainfo.log | uniq -c | awk '{sum+=$1} END {printf "Rata-rata serangan adalah sebanyak %f requests per jam\n", (sum-1)/(NR-1)}' > $avgFile
```
Code di atas berfungsi untuk mencari kolom **Request**. Kemudian, dilakukan perhitungan tiap request yang sama (``` uniq -c```). Selanjutnya hasil dari perhitungan ditulis di file ***ratarata.txt***.

> Subsoal 3
``` bash
awk -F':' '{print $1}' log_website_daffainfo.log | sort | uniq -c | sort -rn | awk -F'"' '{print $1 $2}' | awk 'NR==1 {printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", $2, $1}' > $ipFile
```

Code di atas digunakan untuk mencari IP pada setiap serangan. Setelah semua IP didapatkan, akan dihitung tiap IP yang sama dan diurutkan dari jumlah IP terbanyak. Selanjutnya IP yang terbanyak tersebut dituliskan di file ***result.txt** beserta jumlah IP nya.

> Subsoal 4
``` bash
awk '/curl/{print}' log_website_daffainfo.log | awk 'END {print "Ada "NR" requests yang menggunakan curl sebagai user-agent"}' >> $ipFile
```
Code di atas dilakukan untuk mendapatkan User Agent dengan **curl**. Setelah didapatkan semuanya, dituliskan total penyerangan yang menggunakan User Agent tersebut di file **result.txt**.

> Subsoal 5
``` bash
awk '/22\/Jan\/2022:02/ {print}' log_website_daffainfo.log | awk -F':' '{print $1}' | sort | uniq -c | sort -rn | awk '{print $2}' | awk -F'"' '{print ""$1""$2" Jam 2 pagi"}' >> $ipFile
```

Code di atas berfungsi untuk mencari penyerangan yang memiliki format waktu **22/Jan/2022:02...**. Setelah ditemukan semuanya, langkah selanjutnya adalah mencari IP dari setiap serangan yang sudah ditemukan sebelumnya. Lalu, dihitung tiap IP yang sama dan diurutkan dari yang terbanyak. Hasilnya akan dituliskan di file **result.txt**

# Soal 3
## Persoalan

- Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
- Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
- Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
- Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

## Penyelesaian
1. Membuat script pada file minute_log.sh
``` bash
#!/bin/bash

# ===Find And Write Every Mem and Swap size===
MemTotal=$(free -m | awk '/^Mem/{print $2 }')

MemUsed=$(free -m | awk '/^Mem/{print $3 }')

MemFree=$(free -m | awk '/^Mem/{print $4 }')

MemShare=$(free -m | awk '/^Mem/{print $5 }')

MemAvail=$(free -m | awk '/^Mem/{print $6 }')

SwapTotal=$(free -m | awk '/^Swap/{print $2 }')

SwapUsed=$(free -m | awk '/^Swap/{print $3 }')

SwapFree=$(free -m | awk '/^Swap/{print $4 }')
```
code diatas berfungsi untuk mencatat semua ukuran dari Mem(total,used,free,share,dan available) serta
Swap(total,used,dan free)
``` bash
# ===Writing Date and Time===
Time=$(date '+%Y%m%d%H%M%S')
```

code diatas digunakan untuk mengubah nama file sesuai ketentuan yaitu dengan urutan tahun,bulan,hari,jam,menit,dan detik
``` bash
# === Creating direct file===
echo "MemTotal,MemUsed,MemFree,MemShare,MemAvail,SwapTotal,SwapUsed,SwapFree" > metrics_$Time.log

echo "$MemTotal,$MemUsed,$MemFree,$MemShare,$MemAvail,$SwapTotal,$SwapUsed,$SwapFree" >> metrics_$Time.log
```
code diatas berfungsi untuk membuat file baru jika belum ada lalu menambahkan apa yang terdapat di script yang telah dibuat dari masing masing variabel
kemudian memasukan kedalam file tanpa menimpa data yang sudah ada

2. Membuat script untuk mencatat metrics
    - Disini kami menggunakan crontab tetapi belum yakin apakah benar untuk script yang digunakan karena belum terlalu paham

3. Membuat satu script untuk membuat agregasi file log ke satuan jam.
    - Kami tidak dapat menyelesaikan sub soal ketiga dikarenakan belum terlalu paham bagaimana caranya. 
.
4. Memastikan semua file log hanya dapat dibaca oleh user pemilik file
    - Kami tidak dapat menyelesaikan sub soal ketiga dikarenakan belum terlalu paham bagaimana caranya. 