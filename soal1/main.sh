#! /bin/bash
printf "\n ====== Welcome to Main Page ======\n"

printf "Please enter your username: "
read username
printf "Please enter your password: "
read -s password
printf "\n"

# login status
isLogin=0

# create log.txt if not exist
if [ ! -f "log.txt" ]
then
  touch log.txt
fi

# check login status
if grep -wq "password: $password" ./users/user.txt
then
    date +"%D %T LOGIN: INFO User $username logged in." >> log.txt
    isLogin=1
else
    date +"%D %T LOGIN: ERROR Failed login attempt on user $username." >> log.txt # Password not same
    exit 1
fi

# success login
if [ $isLogin==1 ]; then
  # choose menu
  read -p "Choose your option: (att / dl 1 - ~) or (exit) " option num
  num="${num:=0}"
  
    # att
    if [ $option == "att" ]; then
      grep -wc "LOGIN:.*$username" log.txt
    fi

    # dl 
    if [ $num -gt 0 ]; then
      dir_name="$(date +"%Y-%m-%d")_$username"
      mkdir $dir_name
      cd $dir_name

      # download img
      for ((i=1;i<=$num;i++))
        do
          if [ $i -lt 10 ]; then
            wget -O "PIC_0$i.jpeg" "https://loremflickr.com/320/240"
          else
            wget -O "PIC_$i.jpeg" "https://loremflickr.com/320/240"
          fi
      done

      cd ..
    
      # zip folder
      if [ -f "$(date +"%Y-%m-%d")_$username.zip" ]
      then
        unzip -P $password $dir_name".zip"
        rm $dir_name".zip"
        zip -P $password -r "$(date +"%Y-%m-%d")_$username.zip" $dir_name
      else
        zip -P $password -r "$(date +"%Y-%m-%d")_$username.zip" $dir_name
      fi
    fi
fi