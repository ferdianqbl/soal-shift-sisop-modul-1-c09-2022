#! /bin/sh
printf "\n ====== Welcome to register Page ======\n"

printf "Please enter your username: "
read username
printf "Please enter your password: "
read -s password

printf "\n"

# === VALIDATION ===
# i. Minimal 8 karakter
if [ ${#password} -lt 8 ]
then
  printf "Password must be at least 8 characters long\n"
  exit 1
fi

# ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
if [[ ! "$password" =~ [[:upper:]] ]] 
then
  printf "uppercase character not found\n"
  exit 1
fi

if [[ ! "$password" =~ [[:lower:]] ]] 
then
  printf "lowercase character not found\n"
  exit 1
fi

# iii. Alphanumeric
if [[ "$password" =~ [^a-zA-Z0-9] ]] 
then
  printf "password must alphanumeric\n"
  exit 1
fi


# iv. Tidak boleh sama dengan username
if [ "$username" == "$password" ]
then
  printf "username and password must not be same\n"
  exit 1
fi

# === CHECK IF users FOLDER AND user.txt !EXIST ===
if [ ! -d "users" ]
then
  mkdir users
fi

if [ ! -f "log.txt" ]
then
  touch log.txt
fi

cd users

if [ ! -f "user.txt" ]
then
  touch user.txt
fi

# === CHECK IF username EXIST ===
if grep -wq "$username" user.txt
then
    date +"%D %T REGISTER: ERROR User already exists." >> ../log.txt
    exit 1
fi

# === FINAL RESULT ===
echo "username: $username" >> user.txt
echo "password: $password" >> user.txt
echo "" >> user.txt
date +"%D %T REGISTER: INFO User $username registered successfully." >> ../log.txt

